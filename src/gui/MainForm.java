package gui;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import net.iharder.dnd.FileDrop;


public class MainForm extends javax.swing.JFrame {

    private File path;
    private boolean clipboard = false;
    private String textCopied = "";
    public static Scanner scan;
    private final StyledDocument doc = new DefaultStyledDocument();
    
    public MainForm() {
        initComponents();
        setLocationRelativeTo(null);
        initDropMode();
    }
    
    private void initDropMode(){
        new FileDrop(pnlDropable, new FileDrop.Listener() {
            @Override
            public void filesDropped(File[] files) {
                for(File f : files){
                    clipboard=false;
                    btnStart.setEnabled(true);
                    path=f;
                    lblPath.setText(f.getName());
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlDropable = new javax.swing.JPanel();
        lblPath = new javax.swing.JLabel();
        btnStart = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lblPalabra = new javax.swing.JTextPane(doc);
        spnPPM = new javax.swing.JSpinner();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setResizable(false);

        lblPath.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnStart.setText("Comenzar");
        btnStart.setEnabled(false);
        btnStart.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnStartMouseReleased(evt);
            }
        });

        lblPalabra.setEditable(false);
        lblPalabra.setFont(new java.awt.Font("Ubuntu", 0, 20)); // NOI18N
        jScrollPane1.setViewportView(lblPalabra);

        spnPPM.setModel(new javax.swing.SpinnerNumberModel(250, 200, 400, 50));
        spnPPM.setEditor(new javax.swing.JSpinner.NumberEditor(spnPPM, ""));
        spnPPM.setFocusable(false);

        jLabel1.setText("palabras por minuto");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("|");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("|");

        javax.swing.GroupLayout pnlDropableLayout = new javax.swing.GroupLayout(pnlDropable);
        pnlDropable.setLayout(pnlDropableLayout);
        pnlDropableLayout.setHorizontalGroup(
            pnlDropableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDropableLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDropableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addGroup(pnlDropableLayout.createSequentialGroup()
                        .addGroup(pnlDropableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pnlDropableLayout.createSequentialGroup()
                                .addComponent(spnPPM, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblPath, javax.swing.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnStart, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlDropableLayout.setVerticalGroup(
            pnlDropableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDropableLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDropableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPath, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnStart))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDropableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(spnPPM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addContainerGap())
        );

        jMenu1.setText("Archivo");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.SHIFT_MASK));
        jMenuItem1.setText("Cargar del portapapeles");
        jMenuItem1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jMenuItem1MouseReleased(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlDropable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlDropable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 3, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnStartMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStartMouseReleased
        // TODO add your handling code here:
        
        btnStart.setEnabled(false);
        
        final Timer timer = new Timer();
        
        int q = (int) spnPPM.getValue();
        int value = 60000/q;
        

        try {
            
            if(clipboard){
                scan = new Scanner(textCopied);
            }else{
                scan = new Scanner(path);
            }
                
            timer.schedule(new TimerTask(){
            
            @Override
            public void run() {
                if(scan.hasNext()){
                    String s = scan.next();
                    int mitad=s.length()/2;
                    if(s.length()%2==0) mitad--;

                    lblPalabra.setText(s);

                    SimpleAttributeSet set = new SimpleAttributeSet();
                    SimpleAttributeSet center = new SimpleAttributeSet();
                    StyleConstants.setForeground(set, new Color(255, 0, 0));
                    StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
                    StyleConstants.setFontSize(center, 21);
                    doc.setCharacterAttributes(mitad, 1, set, true);
                    lblPalabra.setParagraphAttributes(center, true);
                    
                }else{
                    timer.cancel();
                    btnStart.setEnabled(true);
                    lblPalabra.setText("Terminado.");
                }
            }
            
        }, 0, value);
        } catch (FileNotFoundException ex) {}
        
        
    }//GEN-LAST:event_btnStartMouseReleased

    private void jMenuItem1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem1MouseReleased

        try {
            Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
            Transferable dato = cb.getContents(this);
            
            DataFlavor dataFlavorStringJava = new DataFlavor("application/x-java-serialized-object; class=java.lang.String");
            
            if (dato.isDataFlavorSupported(dataFlavorStringJava)) {
                textCopied = (String) dato.getTransferData(dataFlavorStringJava);
                clipboard=true;
                lblPath.setText("Texto cargado del portapapeles.");
                btnStart.setEnabled(true);
            }else{
                
            }
            
        } catch (ClassNotFoundException ex) {
        } catch (UnsupportedFlavorException ex) {
        } catch (IOException ex) {}
        
    }//GEN-LAST:event_jMenuItem1MouseReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnStart;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextPane lblPalabra;
    private javax.swing.JLabel lblPath;
    private javax.swing.JPanel pnlDropable;
    private javax.swing.JSpinner spnPPM;
    // End of variables declaration//GEN-END:variables
}
